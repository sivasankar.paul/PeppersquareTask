package com.test.app.peppersquaretask;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

public class MainActivity extends AppCompatActivity {

    Toolbar toolbar;
    TabLayout tabs;
    ViewPager view_page;
    Menu_adapter madapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorWhite));
        setSupportActionBar(toolbar);
        setTitle("Home");

        /* findviewbyid */
        tabs = (TabLayout) findViewById(R.id.tab_layout);
        view_page = (ViewPager) findViewById(R.id.pager);

        /* set 3 tabs */
        tabs.addTab(tabs.newTab().setIcon(R.drawable.home_select));
        tabs.addTab(tabs.newTab().setIcon(R.drawable.book_unselect));
        tabs.addTab(tabs.newTab().setIcon(R.drawable.add_unselect));
        tabs.setTabGravity(TabLayout.GRAVITY_FILL);
        tabs.setTabMode(TabLayout.MODE_FIXED);

        view_page.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabs));
        tabs.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                view_page.setCurrentItem(tab.getPosition());
                tabLayout_(tab);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

        madapter = new Menu_adapter(getSupportFragmentManager(), tabs.getTabCount());
        view_page.setAdapter(madapter);

    }

    private void tabLayout_(TabLayout.Tab tab) {
        tabs.getTabAt(0).setIcon(R.drawable.home_unselect);
        tabs.getTabAt(1).setIcon(R.drawable.book_unselect);
        tabs.getTabAt(2).setIcon(R.drawable.add_unselect);
        if (tab.getPosition() == 0) {
            tab.setIcon(R.drawable.home_select);
            setTitle("Home");
        } else if (tab.getPosition() == 1) {
            tab.setIcon(R.drawable.book_select);
            setTitle("Most Popular");
        } else if (tab.getPosition() == 2) {
            tab.setIcon(R.drawable.add_select);
            setTitle("Create");
        }
    }

    class Menu_adapter extends FragmentStatePagerAdapter {
        int mNumofTabs;

        public Menu_adapter(FragmentManager fm, int NumofTabs) {
            super(fm);
            this.mNumofTabs = NumofTabs;
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    Home_fragment fra1 = new Home_fragment(MainActivity.this, true);
                    return fra1;
                case 1:
                    Home_fragment fra2 = new Home_fragment(MainActivity.this, false);
                    return fra2;
                case 2:
                    Create_post fra3 = new Create_post(MainActivity.this);
                    return fra3;
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return this.mNumofTabs;
        }
    }


}
