package com.test.app.peppersquaretask.Task;

import android.os.AsyncTask;

import com.android.volley.RequestQueue;
import com.test.app.peppersquaretask.VolleySingleton;
import com.test.app.peppersquaretask.callback.RequestString_post;
import com.test.app.peppersquaretask.extras.TaskUtils;

import org.json.JSONObject;


/**
 * Created by Windows on 02-03-2015.
 */
public class TaskString_post extends AsyncTask<Void, Void, String> {
    private RequestString_post myComponent;
    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;
    private JSONObject map;

    public TaskString_post(RequestString_post myComponent, JSONObject m) {
        this.map = m;
        this.myComponent = myComponent;
        volleySingleton = VolleySingleton.getInstance();
        requestQueue = volleySingleton.getRequestQueue();
    }

    @Override
    protected String doInBackground(Void... params) {
        String listMovies = TaskUtils.Send_srting(requestQueue, map);
        return listMovies;
    }

    @Override
    protected void onPostExecute(String otp) {
        if (myComponent != null) {
            if (otp != null) {
                myComponent.onString_post(otp);
            }
        }
    }


}
