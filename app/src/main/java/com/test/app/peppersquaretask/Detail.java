package com.test.app.peppersquaretask;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.test.app.peppersquaretask.modal.Article;

public class Detail extends AppCompatActivity implements View.OnClickListener {

    Toolbar toolbar;
    public TextView date, author, title, description, likes;
    ImageView banner_img, bookmark_img;
    Article modal_articles;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            Intent i = getIntent();
            modal_articles = i.getParcelableExtra("article_detail");
        }

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorWhite));
        toolbar.setTitle("Detail");
        setSupportActionBar(toolbar);
        final Drawable upArrow = getResources().getDrawable(R.drawable.back_white);
        upArrow.setColorFilter(getResources().getColor(R.color.colorWhite), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        /* findviewbyid */
        banner_img = (ImageView) findViewById(R.id.banner_img);
        bookmark_img = (ImageView) findViewById(R.id.bookmark_img);
        date = (TextView) findViewById(R.id.date);
        author = (TextView) findViewById(R.id.author);
        title = (TextView) findViewById(R.id.title);
        description = (TextView) findViewById(R.id.description);
        likes = (TextView) findViewById(R.id.likes);
        bookmark_img.setOnClickListener(this);


        if (!Apputil.isEmpty(modal_articles.getCreated_at())) {
            String dat = Apputil.ConvertDateandTimeFromServer(modal_articles.getCreated_at());
            date.setVisibility(View.VISIBLE);
            date.setText(dat);
        } else {
            date.setVisibility(View.GONE);
        }

        if (!Apputil.isEmpty(modal_articles.getAuthor())) {
            author.setVisibility(View.VISIBLE);
            author.setText(modal_articles.getAuthor());
        } else {
            author.setVisibility(View.GONE);
        }

        if (!Apputil.isEmpty(modal_articles.getTitle())) {
            title.setVisibility(View.VISIBLE);
            title.setText(modal_articles.getTitle());
        } else {
            title.setVisibility(View.GONE);
        }

        if (!Apputil.isEmpty(modal_articles.getLikes())) {
            likes.setVisibility(View.VISIBLE);
            likes.setText(modal_articles.getLikes());
        } else {
            likes.setVisibility(View.GONE);
        }

        if (!Apputil.isEmpty(modal_articles.getDescription())) {
            description.setVisibility(View.VISIBLE);
            description.setText(modal_articles.getDescription());
        } else {
            description.setVisibility(View.GONE);
        }

        if (!Apputil.isEmpty(modal_articles.getImage())) {
            Picasso.with(getApplicationContext()).load(modal_articles.getImage().trim().replace(" ", "%20")).placeholder(R.drawable.placeholder).centerCrop().fit().error(R.drawable.banner).into(banner_img);
        }

        if (modal_articles.isPublished()) {
            Picasso.with(getApplicationContext()).load(R.drawable.book_filled).into(bookmark_img);
        } else {
            Picasso.with(getApplicationContext()).load(R.drawable.book_unselect).into(bookmark_img);
        }


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }


    @Override
    public void onClick(View view) {
        if (view == bookmark_img) {
            if (modal_articles.isPublished()) {
                modal_articles.setPublished(false);
            } else {
                modal_articles.setPublished(true);
            }
            if (modal_articles.isPublished()) {
                Picasso.with(getApplicationContext()).load(R.drawable.book_filled).into(bookmark_img);
            } else {
                Picasso.with(getApplicationContext()).load(R.drawable.book_unselect).into(bookmark_img);
            }
        }
    }
}
