package com.test.app.peppersquaretask;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import com.test.app.peppersquaretask.Task.TaskString_post;
import com.test.app.peppersquaretask.callback.RequestString_post;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Create_post extends Fragment implements View.OnClickListener, RequestString_post {

    Context context;
    EditText title, description, category, author, image_only;
    Button btn_publish;
    boolean valid = true;
    String title_str, description_str, category_str, author_str, image_only_str;
    private ProgressDialog pDialog;
    SwitchCompat notify_me;
    boolean publish_article = false;
    JSONArray category_array = new JSONArray();

    public Create_post(Context con) {
        this.context = con;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_create_post, container, false);

        /* findviewbyid */
        title = (EditText) v.findViewById(R.id.title);
        description = (EditText) v.findViewById(R.id.description);
        category = (EditText) v.findViewById(R.id.category);
        author = (EditText) v.findViewById(R.id.author);
        image_only = (EditText) v.findViewById(R.id.image_only);
        btn_publish = (Button) v.findViewById(R.id.btn_publish);
        notify_me = (SwitchCompat) v.findViewById(R.id.notify_me);


        notify_me.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                publish_article = isChecked;
            }
        });

        btn_publish.setOnClickListener(this);


        return v;
    }


    @Override
    public void onClick(View view) {
        if (view == btn_publish) {
            if (validation()) {
                if (Apputil.checkInternetConnection(context)) {
                    List<String> category_list = new ArrayList<String>(Arrays.asList(category_str.trim().split(",")));
                    category_array = new JSONArray();
                    for (int x = 0; x < category_list.size(); x++) {
                        category_array.put(category_list.get(x));
                    }
                    senddata();
                } else {
                    Apputil.No_network_connection(context);
                }
            }
        }
    }

    private boolean validation() {

        valid = true;
        title_str = title.getText().toString().trim();
        description_str = description.getText().toString().trim();
        category_str = category.getText().toString().trim();
        author_str = author.getText().toString().trim();
        image_only_str = image_only.getText().toString().trim();

        if (title_str.length() == 0) {
            title.setError(context.getResources().getString(R.string.error_title));
            valid = false;
        } else {
            title.setError(null);
        }

        if (description_str.length() == 0) {
            description.setError(context.getResources().getString(R.string.error_description));
            valid = false;
        } else {
            description.setError(null);
        }

        if (category_str.length() == 0) {
            category.setError(context.getResources().getString(R.string.error_category));
            valid = false;
        } else {
            category.setError(null);
        }

        if (author_str.length() == 0) {
            author.setError(context.getResources().getString(R.string.error_author));
            valid = false;
        } else {
            author.setError(null);
        }

        if (image_only_str.length() == 0) {
            image_only.setError(context.getResources().getString(R.string.error_image_only));
            valid = false;
        } else {
            image_only.setError(null);
        }

        return valid;
    }


    public void senddata() {
        if (Apputil.checkInternetConnection(context)) {
            btn_publish.setText("Loading...");
            progress_show();
            JSONObject params = new JSONObject();
            try {
                params.put("title", title_str);
                params.put("description", description_str);
                params.put("author", author_str);
                params.put("image", image_only_str);
                params.put("tags", category_array);
                params.put("published", publish_article);
                Log.e("params", "params: " + params);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            new TaskString_post(this, params).execute();
        } else {
            Apputil.No_network_connection(context);
        }
    }

    @Override
    public void onString_post(String response) {
        if (response != null) {
            Toast.makeText(context, "Article posted successfully...", Toast.LENGTH_SHORT).show();
            clearpost();
        }
    }

    private void clearpost() {
        title.setText("");
        description.setText("");
        category.setText("");
        author.setText("");
        image_only.setText("");
        notify_me.setChecked(false);
        btn_publish.setText("Publish");
        progress_hide();
    }

    public void progress_show() {
        pDialog = new ProgressDialog(context);
        pDialog.setMessage("Loading");
        pDialog.show();
    }

    public void progress_hide() {
        if (pDialog.isShowing()) {
            pDialog.hide();
        }
    }


}
