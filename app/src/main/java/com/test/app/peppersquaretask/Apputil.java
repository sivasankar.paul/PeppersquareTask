package com.test.app.peppersquaretask;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.provider.Settings;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by croyon11 on 8/30/2017.
 */

public class Apputil {

    /* checking the intrenet connection */
    public static boolean checkInternetConnection(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm.getActiveNetworkInfo() != null
                && cm.getActiveNetworkInfo().isAvailable()
                && cm.getActiveNetworkInfo().isConnected()) {
                /*Network connecting...*/
            return true;
        } else {
                /*oops!!! no network*/
            //   No_network_connection(context);
            Log.d("TAG", "Internet Connection Not Present");
            return false;
        }
    }

    /* Go to setting page -> weather their is no internet connection */
    public static void No_network_connection(final Context context) {
        android.support.v7.app.AlertDialog.Builder alert_build = new android.support.v7.app.AlertDialog.Builder(context);
        alert_build.setTitle("Network Information");
        alert_build.setMessage("Please check your Internet connection");
        alert_build.setCancelable(false);
        alert_build.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            /* Go to Settings page Intent */
                Intent i = new Intent(Settings.ACTION_SETTINGS);
                context.startActivity(i);
                dialog.dismiss();

            }
        });
        alert_build.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        android.support.v7.app.AlertDialog alert_show = alert_build.create();
        alert_show.show();
    }

    public static void animation(RecyclerView.ViewHolder holder, boolean value) {
        AnimatorSet animatorSet = new AnimatorSet();
        ObjectAnimator translatey = ObjectAnimator.ofFloat(holder.itemView, "translationY", value == true ? 200 : -200, 0);
        translatey.setDuration(1000);
        animatorSet.playTogether(translatey);
        animatorSet.start();
    }


    public static boolean isEmpty(String string) {
        return string == null || string.length() == 0;
    }

    public static String ConvertDateandTimeFromServer(String str) {
        str = str.split("\\.")[0];
        SimpleDateFormat readDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        /*readDate.setTimeZone(TimeZone.getTimeZone("GMT"));*/
        Date date = null;
        try {
            date = readDate.parse(str);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");
        String date_str = dateFormat.format(date);
        DateFormat timeFormat = new SimpleDateFormat("hh:mm a");
        String time_str = timeFormat.format(date);
        String dat = date_str + " " + time_str;
        return dat;
    }

    public static void onUsersignedin(VolleyError error) {
        String json = null;
        NetworkResponse response = error.networkResponse;
        Log.e("volley_error_response", "volley_error_response" + response);
        if (response != null && response.data != null) {
            switch (response.statusCode) {
                case 500:
                    break;
                default:
                    break;
            }
        }
        if (error != null) {
            if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                // mTextError.setText(R.string.error_timeout);
            } else if (error instanceof AuthFailureError) {
                // mTextError.setText(R.string.error_auth_failure);
                //TODO
            } else if (error instanceof ServerError) {
                //  mTextError.setText(R.string.error_auth_failure);
                //TODO
            } else if (error instanceof NetworkError) {
                //  mTextError.setText(R.string.error_network);
                //TODO
            } else if (error instanceof ParseError) {
                //  mTextError.setText(R.string.error_parser);
                //TODO
            }
        }
    }


}
