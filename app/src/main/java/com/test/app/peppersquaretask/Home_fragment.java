package com.test.app.peppersquaretask;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.test.app.peppersquaretask.Task.TaskString_get;
import com.test.app.peppersquaretask.callback.RequestString_get;
import com.test.app.peppersquaretask.modal.Article;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Home_fragment extends Fragment implements RequestString_get {

    Context context;
    RecyclerView articlelist;
    private ProgressDialog pDialog;
    TextView no_aritcle_found;
    ArrayList<Article> modal_articles = new ArrayList<>();
    AritlelistAdapter aritlelistAdapter;
    boolean animation_once = true;
    boolean type;
    Article modal_articles_is;

    public Home_fragment(Context con, boolean type_) {
        this.context = con;
        this.type = type_;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_home_fragment, container, false);

        /* findviewbyid */
        articlelist = (RecyclerView) v.findViewById(R.id.articlelist);
        no_aritcle_found = (TextView) v.findViewById(R.id.no_aritcle_found);
        RecyclerView.LayoutManager re_lay_manager = new LinearLayoutManager(context);
        articlelist.setLayoutManager(re_lay_manager);

        /* load the data - GET method */
        loaddata();

        return v;
    }

    public void loaddata() {
        if (Apputil.checkInternetConnection(context)) {
            progress_show();
            JSONObject params = new JSONObject();
            new TaskString_get(this, params).execute();
        } else {
            Apputil.No_network_connection(context);
        }
    }

    @Override
    public void onString_get(String response) {
        if (response != null && response.length() > 0) {
            try {
                JSONArray jsonArray = new JSONArray(response);
                if (jsonArray.length() > 0) {
                    modal_articles.clear();
                    for (int j = 0; j < jsonArray.length(); j++) {
                        JSONObject liveobj = jsonArray.getJSONObject(j);
                        if (type) {
                            Gson gson = new Gson();
                            Article obj = null;
                            obj = new Article();
                            obj = gson.fromJson(liveobj.toString(), Article.class);
                            modal_articles.add(obj);
                        } else {
                            if (liveobj.getBoolean("published")) {
                                Gson gson = new Gson();
                                Article obj = null;
                                obj = new Article();
                                obj = gson.fromJson(liveobj.toString(), Article.class);
                                modal_articles.add(obj);
                            }
                        }
                    }

                    if (modal_articles.size() > 0) {
                        aritlelistAdapter = new AritlelistAdapter(context, modal_articles);
                        articlelist.setAdapter(aritlelistAdapter);
                        articlelist.setVisibility(View.VISIBLE);
                        no_aritcle_found.setVisibility(View.GONE);
                    } else {
                        articlelist.setVisibility(View.GONE);
                        no_aritcle_found.setVisibility(View.VISIBLE);
                    }

                    progress_hide();

                } else {
                    no_aritcle_found.setVisibility(View.VISIBLE);
                    articlelist.setVisibility(View.GONE);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void progress_show() {
        pDialog = new ProgressDialog(context);
        pDialog.setMessage("Loading");
        pDialog.show();
    }

    public void progress_hide() {
        if (pDialog.isShowing()) {
            pDialog.hide();
        }
    }


    class AritlelistAdapter extends RecyclerView.Adapter<AritlelistAdapter.ViewHolder> {

        ArrayList<Article> modal_articles = new ArrayList<>();

        Context context;
        int previous_position = 0;

        public AritlelistAdapter(Context con, ArrayList<Article> modal_artic) {
            this.context = con;
            this.modal_articles = modal_artic;

        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            public ImageView banner_img, bookmark_img;
            public TextView date, author, title, description, likes;
            LinearLayout item_id;

            public ViewHolder(View itemvi) {
                super(itemvi);
                date = (TextView) itemvi.findViewById(R.id.date);
                author = (TextView) itemvi.findViewById(R.id.author);
                title = (TextView) itemvi.findViewById(R.id.title);
                description = (TextView) itemvi.findViewById(R.id.description);
                likes = (TextView) itemvi.findViewById(R.id.likes);
                banner_img = (ImageView) itemvi.findViewById(R.id.banner_img);
                bookmark_img = (ImageView) itemvi.findViewById(R.id.bookmark_img);
                item_id = (LinearLayout) itemvi.findViewById(R.id.item_id);
            }
        }


        @Override
        public int getItemCount() {
            return modal_articles.size();
        }


        @Override
        public void onBindViewHolder(AritlelistAdapter.ViewHolder holder, final int position) {
            if (animation_once) {
                if (position > previous_position) {
                    Apputil.animation(holder, true);
                } else {
                    Apputil.animation(holder, false);
                }
                previous_position = position;
            }


            if (!Apputil.isEmpty(modal_articles.get(position).getCreated_at())) {
                String dat = Apputil.ConvertDateandTimeFromServer(modal_articles.get(position).getCreated_at());
                holder.date.setVisibility(View.VISIBLE);
                holder.date.setText(dat);
            } else {
                holder.date.setVisibility(View.GONE);
            }

            if (!Apputil.isEmpty(modal_articles.get(position).getAuthor())) {
                holder.author.setVisibility(View.VISIBLE);
                holder.author.setText(modal_articles.get(position).getAuthor());
            } else {
                holder.author.setVisibility(View.GONE);
            }

            if (!Apputil.isEmpty(modal_articles.get(position).getTitle())) {
                holder.title.setVisibility(View.VISIBLE);
                holder.title.setText(modal_articles.get(position).getTitle());
            } else {
                holder.title.setVisibility(View.GONE);
            }

            if (!Apputil.isEmpty(modal_articles.get(position).getLikes())) {
                holder.likes.setVisibility(View.VISIBLE);
                holder.likes.setText(modal_articles.get(position).getLikes());
            } else {
                holder.likes.setVisibility(View.GONE);
            }

            if (!Apputil.isEmpty(modal_articles.get(position).getDescription())) {
                holder.description.setVisibility(View.VISIBLE);
                holder.description.setText(modal_articles.get(position).getDescription());
            } else {
                holder.description.setVisibility(View.GONE);
            }

            if (!Apputil.isEmpty(modal_articles.get(position).getImage())) {
                Picasso.with(context).load(modal_articles.get(position).getImage().trim().replace(" ", "%20")).placeholder(R.drawable.placeholder).centerCrop().fit().error(R.drawable.banner).into(holder.banner_img);
            }

            if (modal_articles.get(position).isPublished()) {
                Picasso.with(context).load(R.drawable.book_filled).into(holder.bookmark_img);
            } else {
                Picasso.with(context).load(R.drawable.book_unselect).into(holder.bookmark_img);
            }

            holder.bookmark_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (modal_articles.get(position).isPublished()) {
                        modal_articles.get(position).setPublished(false);
                    } else {
                        modal_articles.get(position).setPublished(true);
                    }
                    animation_once = false;
                    notifyDataSetChanged();
                }
            });

            holder.item_id.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    modal_articles_is = modal_articles.get(position);
                    Intent intent = new Intent(context, Detail.class);
                    intent.putExtra("article_detail", modal_articles_is);
                    startActivity(intent);
                }
            });

        }

        @Override
        public AritlelistAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View create_view = LayoutInflater.from(parent.getContext()).inflate(R.layout.articleadapterlayout, parent, false);
            ViewHolder VH = new ViewHolder(create_view);
            return VH;
        }

    }


}
