package com.test.app.peppersquaretask.modal;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by croyon11 on 10/14/2017.
 */

public class Article implements Parcelable {

    String created_at;
    String description;
    String id;
    String image;
    String author;
    String likes;
    String title;
    boolean published;


    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getLikes() {
        return likes;
    }

    public void setLikes(String likes) {
        this.likes = likes;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isPublished() {
        return published;
    }

    public void setPublished(boolean published) {
        this.published = published;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.created_at);
        dest.writeString(this.description);
        dest.writeString(this.id);
        dest.writeString(this.image);
        dest.writeString(this.author);
        dest.writeString(this.likes);
        dest.writeString(this.title);
        dest.writeByte(this.published ? (byte) 1 : (byte) 0);
    }

    public Article() {
    }

    protected Article(Parcel in) {
        this.created_at = in.readString();
        this.description = in.readString();
        this.id = in.readString();
        this.image = in.readString();
        this.author = in.readString();
        this.likes = in.readString();
        this.title = in.readString();
        this.published = in.readByte() != 0;
    }

    public static final Creator<Article> CREATOR = new Creator<Article>() {
        @Override
        public Article createFromParcel(Parcel source) {
            return new Article(source);
        }

        @Override
        public Article[] newArray(int size) {
            return new Article[size];
        }
    };
}
