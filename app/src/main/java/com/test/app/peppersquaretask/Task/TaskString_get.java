package com.test.app.peppersquaretask.Task;

import android.os.AsyncTask;

import com.android.volley.RequestQueue;
import com.test.app.peppersquaretask.VolleySingleton;
import com.test.app.peppersquaretask.callback.RequestString_get;
import com.test.app.peppersquaretask.extras.TaskUtils;

import org.json.JSONObject;


/**
 * Created by Windows on 02-03-2015.
 */
public class TaskString_get extends AsyncTask<Void, Void, String> {
    private RequestString_get myComponent;
    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;
    private JSONObject map;

    public TaskString_get(RequestString_get myComponent, JSONObject m) {
        this.map = m;
        this.myComponent = myComponent;
        volleySingleton = VolleySingleton.getInstance();
        requestQueue = volleySingleton.getRequestQueue();
    }

    @Override
    protected String doInBackground(Void... params) {
        String listMovies = TaskUtils.get_String(requestQueue, map);
        return listMovies;
    }

    @Override
    protected void onPostExecute(String otp) {
        if (myComponent != null) {
            if (otp != null) {
                myComponent.onString_get(otp);
            }
        }
    }


}
